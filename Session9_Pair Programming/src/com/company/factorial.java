package com.company;

public class factorial {
    public int factorials(int n)
    {
        if (n == 0)
            return 1;

        return n*factorials(n-1);
    }
}
